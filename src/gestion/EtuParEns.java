/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestion;

import java.util.ArrayList;

/**
 *
 * @author laura
 */
public class EtuParEns {

    private String c;
    private String n;
    private int nbEtu;
    private int nbEtuPrev;
    private Enseignement enseignement;

    public EtuParEns(Enseignement enseignement) {
        this.enseignement = enseignement;
        this.c = enseignement.getCode();
        this.n = enseignement.getNom();
        this.nbEtu = 0;
        this.nbEtuPrev = 0;
    }

    public EtuParEns() {
        this.c = null;
        this.n = null;
        this.nbEtu = 0;
        this.nbEtuPrev = 0;
    }

    public Enseignement getEnseignement() {
        return enseignement;
    }

    public void setEnseignement(Enseignement enseignement) {
        this.enseignement = enseignement;
    }

    public String getC() {
        return c;
    }

    public void setC(String code) {
        this.c = code;
    }

    public String getN() {
        return n;
    }

    public void setN(String nom) {
        this.n = nom;
    }

    public int getNbEtu() {
        return nbEtu;
    }

    public void setNbEtu(int nbEtu) {
        this.nbEtu = nbEtu;
    }

    public int getNbEtuPrev() {
        return nbEtuPrev;
    }

    public void setNbEtuPrev(int nbEtuPrev) {
        this.nbEtuPrev = nbEtuPrev;
    }

}
