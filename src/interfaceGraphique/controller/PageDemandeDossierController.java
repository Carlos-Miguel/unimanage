/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaceGraphique.controller;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author laura, océane, carlos
 */
public class PageDemandeDossierController implements Initializable {

    // Fichiers et répertoire
    private static String repertoire;
    private static String fichierEtudiant;
    private static String fichierEnseignement;
    private static String fichierMention;
    private static String fichierParcours;

    private String status;
    private static final String dir = "Directeur d\'étude";
    private static final String sec = "Secrétariat pédagogique";
    private static final String bur = "Bureau d'examen";
    private static final String res = "Responsable de mention";
    private String nomRep;

    // Bouton
    @FXML
    private Button ButtonOui;
    @FXML
    private Button ButtonNon;
    @FXML
    private Button buttonValider;

    // Panel
    @FXML
    private AnchorPane panelSaisirRep;

    // TextField fichiers
    @FXML
    private TextField textFieldRep;
    @FXML
    private TextField textFieldFichierEtudiant;
    @FXML
    private TextField textFieldFichierEnseignement;
    @FXML
    private TextField textFieldFichierMention;
    @FXML
    private TextField textFieldFichierParcours;

    // Gestion des paramètres
    @FXML
    private Button buttonParametre;
    @FXML
    private Button buttonParametre1;
    @FXML
    private AnchorPane panelParametre;
    @FXML
    private AnchorPane panelCacher;
    @FXML
    private Button buttonDeconnexion;
    
    // Label
    @FXML
    private Label labelFichierEtudiant;
    @FXML
    private Label labelFichierEnseignement;
    @FXML
    private Label labelFichierMention;
    @FXML
    private Label labelFichierParcours;
    @FXML
    private Label labelBonjour;
    @FXML
    private Label labelErreurRep;
    @FXML
    private Label labelRepExistePas;

    /**
     * Initializes the controller class.
     *
     * @param url (url)
     * @param rb (ResourceBundle)
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Initialiser fichier 
        repertoire = getPath() + "Données/";
        fichierEtudiant = repertoire + "etudiants.csv";
        fichierEnseignement = repertoire + "enseignements.csv";
        fichierMention = repertoire + "mentions.csv";
        fichierParcours = repertoire + "parcours.csv";

        // Cacher panel
        panelSaisirRep.setVisible(false);
        labelErreurRep.setVisible(false);
        labelRepExistePas.setVisible(false);
        panelParametre.setVisible(false);
        panelCacher.setVisible(false);
        buttonParametre1.setVisible(false);

        labelFichierEtudiant.setVisible(false);
        labelFichierEnseignement.setVisible(false);
        labelFichierMention.setVisible(false);
        labelFichierParcours.setVisible(false);
    }

    /**
     * Récupère le chemin
     * @return 
     */
    private static String getPath() {
        String path = PageDemandeDossierController.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        // Dans le .jar enleve "ProjetJava.jar" du chemin
        // sur l'ide enleve "/"
        String absolutePath = path.substring(0, path.lastIndexOf("/"));
        // Dans le .jar enleve "/dist" du chemin -> a enlever apres si on veux que les données
        // soit dans le meme dossier que le .jar
        // sur l'ide enleve "/classes"
        String[] verif = absolutePath.split("/");
        if (verif[verif.length - 1].equals("classes")) {
            absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            verif = absolutePath.split("/");
            //Si il y'a aussi "/build" l'enleve du chemin (pour l'ide)
            if (verif[verif.length - 1].equals("build")) {
                absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            }
        }
        absolutePath += "/";
        try {
            absolutePath = URLDecoder.decode(absolutePath, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(PageCreerCompteController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //absolutePath += CHEMINDEFAULT;
        //System.out.println("Repertoire données : "+absolutePath);
        return absolutePath;
    }

    /**
     * Permet de modifier la valeur du label labelBonjour
     *
     * @param str (String)
     */
    public void setLabelBonjour(String str) {
        labelBonjour.setText(str);
    }

    /**
     * Permet de modifier la valeur de status
     *
     * @param str (String)
     */
    public void setStatus(String str) {
        this.status = str;
    }

    /**
     * Permet de rendre visible le panel panelSaisirRep
     */
    public void ButtonOui() {
        panelSaisirRep.setVisible(true);
    }

    /**
     * Permet d'ouvrir la bonne page d'accueil En fonction du status sélectionné
     *
     * @throws IOException
     */
    public void ButtonNon() throws IOException {
        if (this.status.equals(dir)) {
            Stage source = (Stage) ButtonNon.getScene().getWindow();
            source.close();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceGraphique/fxml/DirAccueil.fxml"));
            Parent root = loader.load();

            DirAccueilController pageDirAccueil = loader.getController();
            pageDirAccueil.setRepertoire(repertoire);
            pageDirAccueil.setFichierEtudiant(fichierEtudiant);
            pageDirAccueil.setFichierEnseignement(fichierEnseignement);
            pageDirAccueil.setFichierMention(fichierMention);
            pageDirAccueil.setFichierParcours(fichierParcours);
            pageDirAccueil.initialiserLesRestaure();
            pageDirAccueil.listeEtudiants();
            pageDirAccueil.initialiserComboBox();

            Stage mainStage = new Stage();
            Scene scene = new Scene(root);
            mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
            mainStage.setTitle("Université");
            mainStage.setResizable(false);
            mainStage.setScene(scene);
            mainStage.show();
        } else {
            if (this.status.equals(sec)) {
                Stage source = (Stage) ButtonNon.getScene().getWindow();
                source.close();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceGraphique/fxml/SecAccueil.fxml"));
                Parent root = loader.load();

                SecAccueilController pageSecAccueil = loader.getController();
                pageSecAccueil.setRepertoire(repertoire);
                pageSecAccueil.setFichierEtudiant(fichierEtudiant);
                pageSecAccueil.setFichierEnseignement(fichierEnseignement);
                pageSecAccueil.setFichierMention(fichierMention);
                pageSecAccueil.setFichierParcours(fichierParcours);
                pageSecAccueil.initialiserLesRestaure();
                pageSecAccueil.initialiserComboBox();
                pageSecAccueil.initialiserTableauUE();

                Stage mainStage = new Stage();
                Scene scene = new Scene(root);
                mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
                mainStage.setTitle("Université");
                mainStage.setResizable(false);
                mainStage.setScene(scene);
                mainStage.show();
            } else {
                if (this.status.equals(bur)) {
                    Stage source = (Stage) ButtonNon.getScene().getWindow();
                    source.close();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceGraphique/fxml/BurAccueil.fxml"));
                    Parent root = loader.load();

                    BurAccueilController pageBurAccueil = loader.getController();
                    pageBurAccueil.setRepertoire(repertoire);
                    pageBurAccueil.setFichierEtudiant(fichierEtudiant);
                    pageBurAccueil.setFichierEnseignement(fichierEnseignement);
                    pageBurAccueil.setFichierMention(fichierMention);
                    pageBurAccueil.setFichierParcours(fichierParcours);
                    pageBurAccueil.initialiserLesRestaure();
                    pageBurAccueil.initialiserComboBox();
                    pageBurAccueil.initialiserTableauUE();

                    Stage mainStage = new Stage();
                    Scene scene = new Scene(root);
                    mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
                    mainStage.setTitle("Université");
                    mainStage.setResizable(false);
                    mainStage.setScene(scene);
                    mainStage.show();
                }
                else{
                    Stage source = (Stage) ButtonNon.getScene().getWindow();
                    source.close();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceGraphique/fxml/ResAccueil.fxml"));
                    Parent root = loader.load();

                    ResAccueilController pageResAccueil = loader.getController();
                    pageResAccueil.setRepertoire(repertoire);
                    pageResAccueil.setFichierEtudiant(fichierEtudiant);
                    pageResAccueil.setFichierEnseignement(fichierEnseignement);
                    pageResAccueil.setFichierMention(fichierMention);
                    pageResAccueil.setFichierParcours(fichierParcours);
                    pageResAccueil.initialiserLesRestaure();
                    pageResAccueil.initialiserTableauMention();

                    Stage mainStage = new Stage();
                    Scene scene = new Scene(root);
                    mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
                    mainStage.setTitle("Université");
                    mainStage.setResizable(false);
                    mainStage.setScene(scene);
                    mainStage.show();
                }
            }
        }
    }

    /**
     * permet de choisir un répertoire précis avec les noms des fichiers
     *
     * @throws IOException
     */
    public void validerAvecSonRep() throws IOException {
        // Récupérer noms des fichiers + répertoire
        this.repertoire = textFieldRep.getText();
        this.fichierEtudiant = repertoire + "/" + textFieldFichierEtudiant.getText() + ".csv";
        this.fichierEnseignement = repertoire + "/" + textFieldFichierEnseignement.getText() + ".csv";
        this.fichierMention = repertoire + "/" + textFieldFichierMention.getText() + ".csv";
        this.fichierParcours = repertoire + "/" + textFieldFichierParcours.getText() + ".csv";

        // Cacher label
        labelErreurRep.setVisible(false);
        labelRepExistePas.setVisible(false);
        labelFichierEtudiant.setVisible(false);
        labelFichierEnseignement.setVisible(false);
        labelFichierMention.setVisible(false);
        labelFichierParcours.setVisible(false);

        if (repertoire.equals("")) {
            labelErreurRep.setVisible(true);
        } else {
            File directory = new File(repertoire);
            File fEtud = new File(fichierEtudiant);
            File fEns = new File(fichierEnseignement);
            File fMen = new File(fichierMention);
            File fPar = new File(fichierParcours);
            if (!directory.exists() || !fEtud.exists() || !fEns.exists() || !fMen.exists() || !fPar.exists()) {
                if (!directory.exists()) {
                    labelRepExistePas.setVisible(true);
                }
                if (!fEtud.exists()) {
                    //System.out.println("Le fichier etudiant n'existe pas");
                    labelFichierEtudiant.setVisible(true);
                }
                if (!fEns.exists()) {
                    //System.out.println("Le fichier enseignement n'existe pas");
                    labelFichierEnseignement.setVisible(true);
                }
                if (!fMen.exists()) {
                    //System.out.println("Le fichier mention n'existe pas");
                    labelFichierMention.setVisible(true);
                }
                if (!fPar.exists()) {
                    //System.out.println("Le fichier parcours n'existe pas");
                    labelFichierParcours.setVisible(true);
                }
            } else {
                ButtonNon();
            }
        }
    }

    /**
     * afficher les boutons "se déconnecter" , "modifier son mot de passe",
     * "modifier son identifiant"
     */
    public void afficherParametre() {
        panelParametre.setVisible(true);
        panelCacher.setVisible(true);
        buttonParametre.setVisible(false);
        buttonParametre1.setVisible(true);
    }

    /**
     * cacher les boutons "se déconnecter" , "modifier son mot de passe",
     * "modifier son identifiant"
     */
    public void cacherParametre() {
        panelParametre.setVisible(false);
        panelCacher.setVisible(false);
        buttonParametre.setVisible(true);
        buttonParametre1.setVisible(false);
    }

    /**
     * se déconnecter
     *
     * @throws IOException
     */
    public void deconnexion() throws IOException {
        Stage source = (Stage) buttonDeconnexion.getScene().getWindow();
        source.close();
        Parent root = FXMLLoader.load(getClass().getResource("/interfaceGraphique/fxml/Page.fxml"));
        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }
}
