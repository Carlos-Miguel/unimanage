# University Management Java App

In 2021, as part of our Java course, I collaborated with two classmates to develop an application for university management. This application features four distinct roles, each with its own specific functionalities. This project marked my first experience working in a group and my first substantial project in Java.

* Login page

![Home](src/ressources/images/screen/homepage.png)

* Create Account page

![Sign](src/ressources/images/screen/sign.png)

* Home page of the "Study Director" role
  
![Director screen](src/ressources/images/screen/directeur.png)

![Director screen 2](src/ressources/images/screen/directeur2.png)

* Homepage of the "Educational Secretariat" role
  
![Secretariat screen](src/ressources/images/screen/secretariat.png)

![Secretariat screen 2](src/ressources/images/screen/secretariat2.png)

![Secretariat screen 3](src/ressources/images/screen/secretariat3.png)

* Homepage of the "Head of Mention" role

![Head of Mention screen](src/ressources/images/screen/responsable.png)

![Head of Mention screen 2](src/ressources/images/screen/responsable2.png)

![Head of Mention screen 3](src/ressources/images/screen/responsable3.png)

* Homepage of the "Examination Office" role

![Examination Office screen](src/ressources/images/screen/bureau.png)

![Examination Office screen 2](src/ressources/images/screen/bureau2.png)