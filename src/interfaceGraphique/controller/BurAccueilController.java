package interfaceGraphique.controller;

import gestion.Enseignement;
import gestion.Etudiant;
import gestion.Mention;
import gestion.Parcours;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author laura, océane, carlos
 */
public class BurAccueilController implements Initializable {

    // Fichiers et répertoire
    private static String repertoire;
    private static String fichierEtudiant;
    private static String fichierEnseignement;
    private static String fichierMention;
    private static String fichierParcours;

    // Variables pour les restaure
    private ArrayList<Etudiant> restaureEtudiant;
    private ArrayList<Enseignement> restaureEnseignement;
    private ArrayList<Mention> restaureMention;
    private ArrayList<Parcours> restaureParcours;

    private String code;
    private String ue;
    private String mention;
    private String parcours;
    private ArrayList<Enseignement> listeEnseignement = new ArrayList<>();
    private ObservableList<Enseignement> tableauEnseignement = FXCollections.observableArrayList();

    // ComboBox 
    @FXML
    private ComboBox comboBoxCode;
    @FXML
    private ComboBox comboBoxUE;
    @FXML
    private ComboBox comboBoxParcours;
    @FXML
    private ComboBox comboBoxMention;

    // Tableau
    @FXML
    private TableColumn columnCode;
    @FXML
    private TableColumn columnNom;
    @FXML
    private TableView table;

    // Bouton
    @FXML
    private Button buttonAnnuler;

    // Gestion parametres
    @FXML
    private AnchorPane panelCacher;
    @FXML
    private AnchorPane panelParametre;
    @FXML
    private Button buttonParametre;
    @FXML
    private Button buttonParametre1;
    @FXML
    private Button buttonDeconnexion;

    /**
     * Initializes the controller class.
     * @param url (url)
     * @param rb (ResourceBundle)
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tableAction();

        // Initialiser fichier 
        repertoire = "Données\\TEST\\";
        fichierEtudiant = repertoire + "etudiants.csv";
        fichierEnseignement = repertoire + "enseignements.csv";
        fichierMention = repertoire + "mentions.csv";
        fichierParcours = repertoire + "parcours.csv";

        // Initialiser colonne tableau
        // Initialisation du tableau
        columnCode.setCellValueFactory(new PropertyValueFactory<>("code"));
        columnNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        table.setItems(null);

        // Cacher panel/label
        panelCacher.setVisible(false);
        buttonParametre1.setVisible(false);
        panelParametre.setVisible(false);

    }

    /**
     * Modifier l'attribut repertoire
     * @param str (String)
     */
    public void setRepertoire(String str) {
        repertoire = str;
    }

    /**
     * Modifier l'attribut fichierRepertoire
     * @param str (String)
     */
    public void setFichierEtudiant(String str) {
        fichierEtudiant = str;
    }

    /**
     * Modifier l'attribut fichierEnseignement
     * @param str (String)
     */
    public void setFichierEnseignement(String str) {
        fichierEnseignement = str;
    }

    /**
     * Modifier l'attribut fichierMention
     * @param str (String)
     */
    public void setFichierMention(String str) {
        fichierMention = str;
    }

    /**
     *
     * @param str (String)
     */
    public void setFichierParcours(String str) {
        fichierParcours = str;
    }

    /**
     * Initialise les attributs restaureEtudiant, restaureEnseignement, restaureMention, restaureParcours
     */
    public void initialiserLesRestaure() {
        restaureEtudiant = Etudiant.restaure(restaureEnseignement, restaureParcours, fichierEtudiant);
        restaureEnseignement = Enseignement.restaure(fichierEnseignement);
        restaureMention = Mention.restaure(restaureEnseignement, fichierMention);
        restaureParcours = Parcours.restaureP(restaureEnseignement, restaureMention, fichierParcours);
    }

    /**
     * Initialise les comboBox
     */
    public void initialiserComboBox() {
        // Initialisation ComboBox code
        comboBoxCode.setItems(FXCollections.observableArrayList(Enseignement.listeEnsCodeString(restaureEnseignement)));
        comboBoxCode.setValue("Code UE");

        // Initialiser le ComboBox UE avec toutes les UE
        comboBoxUE.setItems(FXCollections.observableArrayList(Enseignement.listeNomEns(restaureEnseignement)));
        comboBoxUE.setValue("UE");

        // Initialiser le ComboBox Parcours avec tous les parcours
        comboBoxParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcours(restaureParcours)));
        comboBoxParcours.setValue("Parcours");

        // Initialiser le ComboBox Mention avec toutes les mentions
        comboBoxMention.setItems(FXCollections.observableArrayList(Mention.nomMentions(restaureMention)));
        comboBoxMention.setValue("Mention");
    }

    /**
     * Initialiser le tableau avec les enseignements
     */
    public void initialiserTableauUE() {
        tableauEnseignement.clear();
        listeEnseignement = Enseignement.restaure(fichierEnseignement);
        for (Enseignement e : listeEnseignement) {
            tableauEnseignement.add(e);
        }
        table.setItems(tableauEnseignement);
    }
    
    /**
     * Retourne une arraylist d'enseignement pour une mention donnée
     *
     * @param ens
     * @param m
     * @return
     */
    public ArrayList<Enseignement> enseignementsPourMention(ArrayList<Enseignement> ens, String m) {
        if ((m != null) && (ens != null)) {
            ArrayList<Enseignement> listeTemp = new ArrayList<>();
            ArrayList<Enseignement> listeMention = new ArrayList<>();
            listeMention.clear();
            listeMention = Parcours.listeEnsMention(restaureParcours, m);
            for (Enseignement e : ens) {
                for (Enseignement e2 : listeMention) {
                    if (e.getCode().equals(e2.getCode())) {
                        listeTemp.add(e);
                    }
                }
            }
            ens.clear();
            ens.addAll(listeTemp);
            return ens;
        }
        return null;
    }

    /**
     * Retourne une arraylist d'enseignement pour un parcours donné
     * @param ens
     * @param p
     * @return 
     */
    public ArrayList<Enseignement> enseignementsPourParcours(ArrayList<Enseignement> ens, String p) {
        if ((p != null) && (ens != null)) {
            ArrayList<Enseignement> listeTemp = new ArrayList<>();
            ArrayList<Enseignement> listeParcours = new ArrayList<>();
            listeParcours = Parcours.listeEnsParcoursSansMention(restaureParcours, p);
            for (Enseignement e : ens) {
                for (Enseignement e2 : listeParcours) {
                    if (e.getCode().equals(e2.getCode())) {
                        listeTemp.add(e);
                    }
                }
            }
            ens.clear();
            ens.addAll(listeTemp);
            return ens;
        }
        return null;
    }

    /**
     * Retourne une arraylist d'enseignement pour un nom d'enseignement donnée
     * @param ens
     * @param n
     * @return 
     */
    public ArrayList<Enseignement> enseignementsPourNom(ArrayList<Enseignement> ens, String n) {
        if ((n != null) && (ens != null)) {
            ens = Enseignement.listeEnsPourNom(ens, n);
            return ens;
        }
        return null;
    }

    /**
     * Retourne une arraylist d'enseignement pour un code d'enseignement donnée
     * @param ens
     * @param c
     * @return 
     */
    public ArrayList<Enseignement> enseignementsPourCode(ArrayList<Enseignement> ens, String c) {
        if ((c != null) && (ens != null)) {
            ens = Enseignement.listeEnsCode(ens, c);
            return ens;
        }
        return null;
    }

    /**
     * Afficher les enseignements dans le tableau par rapport à une arraylist d'enseignement
     * @param ens 
     */
    public void effectuerTrie(ArrayList<Enseignement> ens) {
        tableauEnseignement.clear();
        if(ens!=null){
            for (Enseignement e : ens) {
                tableauEnseignement.add(e);
            }
        }        
        table.setItems(tableauEnseignement);
    }

    /**
     * Adapte le comboBox des parcours en fonction d'une mention
     * @param m
     * @param nom 
     */
    public void trierComboBoxParcours(String m, String nom) {
        comboBoxParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcoursMention(restaureParcours, m)));
        comboBoxParcours.setValue(nom);
    }
    
    /**
     * Modifier comboboxNom par rapport à un Mention
     * @param m
     * @param nom 
     */
    public void trierComboBoxNomM(String m, String nom) {
        comboBoxUE.setItems(FXCollections.observableArrayList(Mention.nomEnseignementPourMention(restaureMention, restaureParcours, m)));
        comboBoxUE.setValue(nom);
    }
    
    /**
     * Modifier comboboxNom par rapport à un Parcours
     * @param p
     * @param nom 
     */
    public void trierComboBoxNomP(String p, String nom) {
        comboBoxUE.setItems(FXCollections.observableArrayList(Parcours.nomEnseignementPourParcours(restaureParcours, p)));
        comboBoxUE.setValue(nom);
    }
    
    /**
     * Modifier comboboxCode par rapport à une mention
     * @param m
     * @param nom 
     */
    public void trierComboBoxCodeM(String m, String nom) {
        comboBoxCode.setItems(FXCollections.observableArrayList(Mention.codeEnseignementPourMention(restaureMention, restaureParcours, m)));
        comboBoxCode.setValue(nom);
    }
    
    /**
     * Modifier comboboxCode par rapport à un parcours
     * @param p
     * @param nom 
     */
    public void trierComboBoxCodeP(String p, String nom) {
        comboBoxCode.setItems(FXCollections.observableArrayList(Parcours.codeEnseignementPourParcours(restaureParcours, p)));
        comboBoxCode.setValue(nom);
    }
    
    /**
     * Modifier comboboxCode par rapport à une mention
     * @param n
     * @param nom 
     */
    public void trierComboBoxCodeN(String n, String nom) {
        comboBoxCode.setItems(FXCollections.observableArrayList(Enseignement.listeEnsCodeStringCodeDonne(restaureEnseignement, n)));
        comboBoxCode.setValue(nom);
    }
    
    /**
     * Trie une liste d'enseignement par rapport à une mention récupérer dans un comboBox
     */
    public void trieMention() {
        listeEnseignement = Enseignement.restaure(fichierEnseignement);

        // Récupérer valeur dans comboBox
        this.mention = (String) comboBoxMention.getSelectionModel().getSelectedItem();
        this.parcours = (String) comboBoxParcours.getSelectionModel().getSelectedItem();
        this.ue = (String) comboBoxUE.getSelectionModel().getSelectedItem();
        this.code = (String) comboBoxCode.getSelectionModel().getSelectedItem();

        // Effectuer le trie
        if ((mention != null) && (parcours != null) && (ue != null) && (code != null)) {
            if (!mention.equals("Mention")) { // L'utilisateur a sélectionné de mention
                listeEnseignement = enseignementsPourMention(listeEnseignement, mention);
                if (!parcours.equals("Parcours")) {
                    listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                    if (!ue.equals("UE")) {
                        listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    } else {
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    }
                } else {
                    if (!ue.equals("UE")) {
                        listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    } else {
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    }
                }
                trierComboBoxParcours(mention, parcours);
                trierComboBoxNomM(mention, ue);
                trierComboBoxCodeM(mention, code);
            }
        }
        effectuerTrie(listeEnseignement);
    }

    /**
     * Trie une liste d'enseignement par rapport à un parcours récupérer dans un comboBox
     */
    public void trieParcours() {
        listeEnseignement = Enseignement.restaure(fichierEnseignement);

        // Récupérer valeur dans comboBox
        this.mention = (String) comboBoxMention.getSelectionModel().getSelectedItem();
        this.parcours = (String) comboBoxParcours.getSelectionModel().getSelectedItem();
        this.ue = (String) comboBoxUE.getSelectionModel().getSelectedItem();
        this.code = (String) comboBoxCode.getSelectionModel().getSelectedItem();

        // Effectuer le trie
        if ((mention != null) && (parcours != null) && (ue != null) && (code != null)) {
            if (!parcours.equals("Parcours")) { 
                listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                if (!mention.equals("Mention")) {
                    listeEnseignement = enseignementsPourMention(listeEnseignement, mention);
                    if (!ue.equals("UE")) {
                        listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    } else {
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    }
                    trierComboBoxNomP(parcours, ue);
                    trierComboBoxCodeP(parcours, code);
                } else {
                    if (!ue.equals("UE")) {
                        listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    } else {
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    }
                    trierComboBoxNomP(parcours, ue);
                    trierComboBoxCodeP(parcours, code);
                }
            }
        }
        effectuerTrie(listeEnseignement);
    }
    
    /**
     * Trie une liste d'enseignement par rapport à un nom d'enseignement récupérer dans un comboBox
     */
    public void trieNom() {
        listeEnseignement = Enseignement.restaure(fichierEnseignement);

        // Récupérer valeur dans comboBox
        this.mention = (String) comboBoxMention.getSelectionModel().getSelectedItem();
        this.parcours = (String) comboBoxParcours.getSelectionModel().getSelectedItem();
        this.ue = (String) comboBoxUE.getSelectionModel().getSelectedItem();
        this.code = (String) comboBoxCode.getSelectionModel().getSelectedItem();

        // Effectuer le trie
        if ((mention != null) && (parcours != null) && (ue != null) && (code != null)) {
            if (!ue.equals("UE")) { 
                listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                if (!mention.equals("Mention")) {
                    listeEnseignement = enseignementsPourMention(listeEnseignement, mention);
                    if (!parcours.equals("Parcours")) {
                        listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                        trierComboBoxCodeN(ue, code);
                    } else {
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                        trierComboBoxCodeN(ue, code);
                    }
                } else {
                    if (!parcours.equals("Parcours")) {
                        listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                        trierComboBoxCodeN(ue, code);
                    } else {
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                        trierComboBoxCodeN(ue, code);
                    }
                }                
            }
        }
        effectuerTrie(listeEnseignement);
    }
    
    /**
     * Trie une liste d'enseignement par rapport à un code d'enseignement récupérer dans un comboBox
     */
    public void trieCodeUE() {
        listeEnseignement = Enseignement.restaure(fichierEnseignement);

        // Récupérer valeur dans comboBox
        this.mention = (String) comboBoxMention.getSelectionModel().getSelectedItem();
        this.parcours = (String) comboBoxParcours.getSelectionModel().getSelectedItem();
        this.ue = (String) comboBoxUE.getSelectionModel().getSelectedItem();
        this.code = (String) comboBoxCode.getSelectionModel().getSelectedItem();

        // Effectuer le trie
        if ((mention != null) && (parcours != null) && (ue != null) && (code != null)) {
            if (!code.equals("Code UE")) { 
                listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                if (!mention.equals("Mention")) {
                    listeEnseignement = enseignementsPourMention(listeEnseignement, mention);
                    if (!parcours.equals("Parcours")) {
                        listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                        if (!ue.equals("UE")) {
                            listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        }
                    } else {
                        if (!ue.equals("UE")) {
                            listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        }
                    }
                } else {
                    if (!parcours.equals("Parcours")) {
                        listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                        if (!ue.equals("UE")) {
                            listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        }
                    } else {
                        if (!ue.equals("UE")) {
                            listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        }
                    }
                }                
            }
        }
        effectuerTrie(listeEnseignement);
    }

    /**
     * Ouvrir fiche d'un enseignement quand on clique sur le tableau
     */
    public void tableAction() {
        table.setRowFactory(tv -> {
            TableRow<Enseignement> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Enseignement enseignement = row.getItem();
                    try {
                        Stage source = (Stage) table.getScene().getWindow();
                        source.close();
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceGraphique/fxml/BurFicheUE.fxml"));
                        Parent root = loader.load();

                        // Permet de sauvegarde les différents éléments d'un étudiant
                        BurFicheUEController pageBurFicheEU = loader.getController();
                        pageBurFicheEU.setEnseignement(enseignement);
                        pageBurFicheEU.setLabelUECodeNom(enseignement.getCode() + " : " + enseignement.getNom());
                        pageBurFicheEU.setRepertoire(repertoire);
                        pageBurFicheEU.setFichierEtudiant(fichierEtudiant);
                        pageBurFicheEU.setFichierEnseignement(fichierEnseignement);
                        pageBurFicheEU.setFichierMention(fichierMention);
                        pageBurFicheEU.setFichierParcours(fichierParcours);
                        pageBurFicheEU.initialiserLesRestaure();
                        pageBurFicheEU.initialiserTableau();

                        // Ouvre la page PageDemandeDossier
                        Stage mainStage = new Stage();
                        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
                        mainStage.setTitle("Université");
                        mainStage.setScene(new Scene(root));
                        mainStage.setResizable(false);
                        mainStage.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
            return row;
        });
    }

    /**
     * Vide tous les comboBox et les textFields
     */
    public void buttonAnnule() {
        comboBoxMention.setValue("Mention");
        comboBoxParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcours(restaureParcours)));
        comboBoxParcours.setValue("Parcours");
        comboBoxUE.setValue("UE");
        comboBoxCode.setItems(FXCollections.observableArrayList(Enseignement.listeEnsCodeString(restaureEnseignement)));
        comboBoxCode.setValue("Code UE");
    }

    /**
     * affiche panel avec les boutons de gestion des identifiants et mot de passe
     * et bouton de déconnexion
     */
    public void afficherParametre() {
        panelParametre.setVisible(true);
        panelCacher.setVisible(true);
        buttonParametre.setVisible(false);
        buttonParametre1.setVisible(true);
    }

    /**
     * cacher panel avec les boutons de gestion des identifiants et mot de passe
     * et bouton de déconnexion
     */
    public void cacherParametre() {
        panelParametre.setVisible(false);
        panelCacher.setVisible(false);
        buttonParametre.setVisible(true);
        buttonParametre1.setVisible(false);
    }

    /**
     * se déconnecter
     * @throws IOException 
     */
    public void deconnexion() throws IOException {
        Stage source = (Stage) buttonDeconnexion.getScene().getWindow();
        source.close();
        Parent root = FXMLLoader.load(getClass().getResource("/interfaceGraphique/fxml/Page.fxml"));
        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }
}
