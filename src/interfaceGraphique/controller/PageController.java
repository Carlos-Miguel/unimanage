package interfaceGraphique.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Label;
import javafx.scene.image.Image;

/**
 *
 * @author laura, océane, carlos
 */
public class PageController {
    
    private String identifiant;
    private String mdp;
    private String status;
    private static String fichierIdentifiant;

    // Bouton
    @FXML
    private Button buttonInscription;
    @FXML
    private Button buttonValider;

    // HyperLink
    @FXML
    private Hyperlink labelMdpOublie;

    // TextField
    @FXML
    private TextField textFieldId;
    @FXML
    private TextField textFieldMdp;

    // Label
    @FXML
    private Label labelErreurConnexion;
    @FXML
    private Label labelIdManquant;
    @FXML
    private Label labelMDPManquant;
    @FXML
    private Label labelPasFichier;

    /**
     * Initializes the controller class
     */
    @FXML
    void initialize() {
        fichierIdentifiant = getPath() + "Données/identifiants.csv";

        labelErreurConnexion.setVisible(false);
        labelIdManquant.setVisible(false);
        labelMDPManquant.setVisible(false);
        labelPasFichier.setVisible(false);
    }

    /**
     * Permet de fermer la première page et d'ouvrir la page permettant de créer
     * un compte
     *
     * @throws IOException
     */
    @FXML
    public void afficherPageCreerCompte() throws IOException {
        Stage source = (Stage) buttonInscription.getScene().getWindow();
        source.close();
        Parent root = FXMLLoader.load(getClass().getResource("/interfaceGraphique/fxml/PageCreerCompte.fxml"));
        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setScene(scene);
        mainStage.show();
    }

    /**
     * Permet d'afficher la page permmettant de modifier son mot de passe
     *
     * @throws IOException
     */
    @FXML
    public void afficherPageModificationMDP() throws IOException {
        Stage source = (Stage) labelMdpOublie.getScene().getWindow();
        source.close();
        Parent root = FXMLLoader.load(getClass().getResource("/interfaceGraphique/fxml/PageModificationMDP.fxml"));
        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }

    /**
     * Permet de vérifier si l'identifiant, le mot de passe et le statut son
     * correcte puis ouvre une nouvelle page en fonction du statut
     * @throws java.io.IOException
     */
    @FXML
    public void verificationIdentite() throws IOException {
        
        identifiant = textFieldId.getText();
        mdp = textFieldMdp.getText();
        labelErreurConnexion.setVisible(false);
        labelIdManquant.setVisible(false);
        labelMDPManquant.setVisible(false);
        labelPasFichier.setVisible(false);

        if (mdp.equals("") || identifiant.equals("")) {
            if (mdp.equals("")) {
                labelMDPManquant.setVisible(true);
            }
            if (identifiant.equals("")) {
                labelIdManquant.setVisible(true);
            }
        } else {
            File fichier = new File(fichierIdentifiant);
            boolean present = false;

            if (fichier.exists()) {
                try {
                    BufferedReader r = new BufferedReader(new InputStreamReader(new FileInputStream(fichierIdentifiant), "UTF-8")); // lis ligne par ligne

                    String l;
                    while ((l = r.readLine()) != null) {
                        String[] ligne = l.split(";");
                        if (ligne[0].equals(identifiant) && ligne[1].equals(mdp)) {
                            present = true;
                            Stage source = (Stage) buttonValider.getScene().getWindow();
                            source.close();

                            FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceGraphique/fxml/PageDemandeDossier.fxml"));
                            Parent root = loader.load();

                            PageDemandeDossierController pageDossier = loader.getController();
                            String idUtilisateur = textFieldId.getText();
                            pageDossier.setLabelBonjour("Bonjour " + idUtilisateur);
                            pageDossier.setStatus(ligne[2]);

                            Stage mainStage = new Stage();
                            mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
                            mainStage.setTitle("Université");
                            mainStage.setScene(new Scene(root));
                            mainStage.setResizable(false);
                            mainStage.show();
                        }
                    }
                    r.close();
                    if (!present) {
                        labelErreurConnexion.setVisible(true);
                    }
                } catch (IOException ex) {
                    System.out.println(ex);
                }
            } else {
                labelPasFichier.setVisible(true);
            }

        }
    }
    
    /**
     * Récupère le chemin
     * @return 
     */
    private static String getPath() {
        String path = PageController.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        // Dans le .jar enleve "ProjetJava.jar" du chemin
        // sur l'ide enleve "/"
        String absolutePath = path.substring(0, path.lastIndexOf("/"));
        // Dans le .jar enleve "/dist" du chemin -> a enlever apres si on veux que les données
        // soit dans le meme dossier que le .jar
        // sur l'ide enleve "/classes"
        String[] verif = absolutePath.split("/");
        if (verif[verif.length - 1].equals("classes")) {
            absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            verif = absolutePath.split("/");
            //Si il y'a aussi "/build" l'enleve du chemin (pour l'ide)
            if (verif[verif.length - 1].equals("build")) {
                absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            }
        }
        absolutePath += "/";
        try {
            absolutePath = URLDecoder.decode(absolutePath, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(PageCreerCompteController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //absolutePath += CHEMINDEFAULT;
        //System.out.println("ABSOLU DE IDENTIFIANT: "+absolutePath);
        return absolutePath;
    }

}
