/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaceGraphique.controller;

import gestion.Enseignement;
import gestion.Etudiant;
import gestion.Mention;
import gestion.Parcours;
import static gestion.Parcours.listeEnsParcoursSansMention;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author laura, océane, carlos
 */
public class SecAccueilController implements Initializable {

    // Fichiers et répertoire
    private static String repertoire;
    private static String fichierEtudiant;
    private static String fichierEnseignement;
    private static String fichierMention;
    private static String fichierParcours;

    // Variables pour les restaure
    private ArrayList<Etudiant> restaureEtudiant;
    private ArrayList<Enseignement> restaureEnseignement;
    private ArrayList<Mention> restaureMention;
    private ArrayList<Parcours> restaureParcours;

    private ArrayList<Enseignement> listeEnseignement = new ArrayList<>();
    private ObservableList<Enseignement> tableauEnseignement = FXCollections.observableArrayList();

    private String mention;
    private String parcours;
    private String ue;
    private String code;

    // ComboBox
    @FXML
    private ComboBox ComboBoxListeMention;
    @FXML
    private ComboBox ComboBoxListeParcours;
    @FXML
    private ComboBox comboBoxListeUE;
    @FXML
    private ComboBox comboBoxListeCodeUE;

    // Composant du tableau
    @FXML
    private TableView table;
    @FXML
    private TableColumn columnNumE;
    @FXML
    private TableColumn columnNom;

    // Button
    @FXML
    private Button vider;
    @FXML
    private Button buttonFermer;

    // Gestion parametres
    @FXML
    private AnchorPane panelCacher;
    @FXML
    private AnchorPane panelParametre;
    @FXML
    private Button buttonParametre;
    @FXML
    private Button buttonParametre1;
    @FXML
    private Button buttonDeconnexion;

    // Button
    @FXML
    private Button buttonVider;

    /**
     * Initializes the controller class.
     *
     * @param url (url)
     * @param rb (ResourceBundle)
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Initialiser les restaure
        restaureEtudiant = new ArrayList<>();
        restaureEnseignement = new ArrayList<>();
        restaureMention = new ArrayList<>();
        restaureParcours = new ArrayList<>();

        // Initialisation du tableau 1
        columnNumE.setCellValueFactory(new PropertyValueFactory<>("code"));
        columnNom.setCellValueFactory(new PropertyValueFactory<>("nom"));

        table.setItems(null);

        // Cacher 
        panelCacher.setVisible(false);
        buttonParametre1.setVisible(false);
        panelParametre.setVisible(false);

        // Autres
        tableAction();
    }

    /**
     * modifier l'attribut repertoire
     *
     * @param str (String)
     */
    public void setRepertoire(String str) {
        repertoire = str;
    }

    /**
     * modifier l'attribut fichierEtudiant
     *
     * @param str (String)
     */
    public void setFichierEtudiant(String str) {
        fichierEtudiant = str;
    }

    /**
     * modifier fichierEnseignement
     *
     * @param str (String)
     */
    public void setFichierEnseignement(String str) {
        fichierEnseignement = str;
    }

    /**
     * modifier l'attribut fichierMention
     *
     * @param str (String)
     */
    public void setFichierMention(String str) {
        fichierMention = str;
    }

    /**
     * modifier l'attribut fichierParcours
     *
     * @param str (String)
     */
    public void setFichierParcours(String str) {
        fichierParcours = str;
    }

    /**
     * intialise les attributs restaureEtudiant, restaureEnseignement,
     * restaureMention, restaureParcours
     */
    public void initialiserLesRestaure() {
        // Initialiser les restaure
        restaureEtudiant = Etudiant.restaure(restaureEnseignement, restaureParcours, fichierEtudiant);
        restaureEnseignement = Enseignement.restaure(fichierEnseignement);
        restaureMention = Mention.restaure(restaureEnseignement, fichierMention);
        restaureParcours = Parcours.restaureP(restaureEnseignement, restaureMention, fichierParcours);
    }

    /**
     * initialise les comboBox
     */
    public void initialiserComboBox() {
        // Initialiser le ComboBox Mention avec toutes les mentions
        ComboBoxListeMention.setItems(FXCollections.observableArrayList(Mention.nomMentions(restaureMention)));
        ComboBoxListeMention.setValue("Mention");

        // Initialiser le ComboBox Parcours avec tous les parcours
        ComboBoxListeParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcours(restaureParcours)));
        ComboBoxListeParcours.setValue("Parcours");

        // Initialiser le ComboBox UE avec toutes les UE
        comboBoxListeUE.setItems(FXCollections.observableArrayList(Enseignement.listeNomEns(restaureEnseignement)));
        comboBoxListeUE.setValue("UE");

        // Initialiser le ComboBox Code UE
        comboBoxListeCodeUE.setItems(FXCollections.observableArrayList(Enseignement.listeEnsCodeString(restaureEnseignement)));
        comboBoxListeCodeUE.setValue("Code UE");
    }

    /**
     * Initialiser le tableau
     */
    public void initialiserTableauUE() {
        tableauEnseignement.clear();
        listeEnseignement = Enseignement.restaure(fichierEnseignement);
        for (Enseignement e : listeEnseignement) {
            tableauEnseignement.add(e);
        }
        table.setItems(tableauEnseignement);
    }

    /**
     * Retourne une arraylist d'enseignement pour une mention donnée
     *
     * @param ens
     * @param m
     * @return
     */
    public ArrayList<Enseignement> enseignementsPourMention(ArrayList<Enseignement> ens, String m) {
        if ((m != null) && (ens != null)) {
            ArrayList<Enseignement> listeTemp = new ArrayList<>();
            ArrayList<Enseignement> listeMention = new ArrayList<>();
            listeMention.clear();
            listeMention = Parcours.listeEnsMention(restaureParcours, m);
            for (Enseignement e : ens) {
                for (Enseignement e2 : listeMention) {
                    if (e.getCode().equals(e2.getCode())) {
                        listeTemp.add(e);
                    }
                }
            }
            ens.clear();
            ens.addAll(listeTemp);
            return ens;
        }
        return null;
    }

    /**
     * Retourne une arraylist d'enseignement pour un parcours donnée
     * @param ens
     * @param p
     * @return 
     */
    public ArrayList<Enseignement> enseignementsPourParcours(ArrayList<Enseignement> ens, String p) {
        if ((p != null) && (ens != null)) {
            ArrayList<Enseignement> listeTemp = new ArrayList<>();
            ArrayList<Enseignement> listeParcours = new ArrayList<>();
            listeParcours = Parcours.listeEnsParcoursSansMention(restaureParcours, p);
            for (Enseignement e : ens) {
                for (Enseignement e2 : listeParcours) {
                    if (e.getCode().equals(e2.getCode())) {
                        listeTemp.add(e);
                    }
                }
            }
            ens.clear();
            ens.addAll(listeTemp);
            return ens;
        }
        return null;
    }

    public ArrayList<Enseignement> enseignementsPourNom(ArrayList<Enseignement> ens, String n) {
        if ((n != null) && (ens != null)) {
            ens = Enseignement.listeEnsPourNom(ens, n);
            return ens;
        }
        return null;
    }

    /**
     * Retourne une arraylist d'enseignement pour un code donnée
     * @param ens
     * @param c
     * @return 
     */
    public ArrayList<Enseignement> enseignementsPourCode(ArrayList<Enseignement> ens, String c) {
        if ((c != null) && (ens != null)) {
            ens = Enseignement.listeEnsCode(ens, c);
            return ens;
        }
        return null;
    }

    /**
     * afficher les enseignements dans un tableau
     * @param ens 
     */
    public void effectuerTrie(ArrayList<Enseignement> ens) {
        tableauEnseignement.clear();
        if(ens!=null){
            for (Enseignement e : ens) {
                tableauEnseignement.add(e);
            }
        }        
        table.setItems(tableauEnseignement);
    }

    /**
     * modifie le comboBox des parcours en fonction d'une mention
     * @param m
     * @param nom 
     */
    public void trierComboBoxParcours(String m, String nom) {
        ComboBoxListeParcours.setItems(FXCollections.observableArrayList(Parcours.nomParcoursMention(restaureParcours, m)));
        ComboBoxListeParcours.setValue(nom);
    }
    
    /**
     * modifie le comboBox des noms d'enseignements en fonction d'une mention
     * @param m
     * @param nom 
     */
    public void trierComboBoxNomM(String m, String nom) {
        comboBoxListeUE.setItems(FXCollections.observableArrayList(Mention.nomEnseignementPourMention(restaureMention, restaureParcours, m)));
        comboBoxListeUE.setValue(nom);
    }
    
    /**
     * Modifier comboboxNom par rapport à un Parcours
     * @param p
     * @param nom 
     */
    public void trierComboBoxNomP(String p, String nom) {
        comboBoxListeUE.setItems(FXCollections.observableArrayList(Parcours.nomEnseignementPourParcours(restaureParcours, p)));
        comboBoxListeUE.setValue(nom);
    }
    
    /**
     * Modifier comboboxCode par rapport à une mention
     * @param m
     * @param nom 
     */
    public void trierComboBoxCodeM(String m, String nom) {
        comboBoxListeCodeUE.setItems(FXCollections.observableArrayList(Mention.codeEnseignementPourMention(restaureMention, restaureParcours, m)));
        comboBoxListeCodeUE.setValue(nom);
    }
    
    /**
     * Modifier comboboxCode par rapport à un parcours
     * @param p
     * @param nom 
     */
    public void trierComboBoxCodeP(String p, String nom) {
        comboBoxListeCodeUE.setItems(FXCollections.observableArrayList(Parcours.codeEnseignementPourParcours(restaureParcours, p)));
        comboBoxListeCodeUE.setValue(nom);
    }
    
    /**
     * Modifier comboboxCode par rapport à un nom d'enseignement
     * @param n
     * @param nom 
     */
    public void trierComboBoxCodeN(String n, String nom) {
        comboBoxListeCodeUE.setItems(FXCollections.observableArrayList(Enseignement.listeEnsCodeStringCodeDonne(restaureEnseignement, n)));
        comboBoxListeCodeUE.setValue(nom);
    }
    
    /**
     * Trie les enseignements en fonctiond d'une mention
     */
    public void trieMention() {
        listeEnseignement = Enseignement.restaure(fichierEnseignement);

        // Récupérer valeur dans comboBox
        this.mention = (String) ComboBoxListeMention.getSelectionModel().getSelectedItem();
        this.parcours = (String) ComboBoxListeParcours.getSelectionModel().getSelectedItem();
        this.ue = (String) comboBoxListeUE.getSelectionModel().getSelectedItem();
        this.code = (String) comboBoxListeCodeUE.getSelectionModel().getSelectedItem();

        // Effectuer le trie
        if ((mention != null) && (parcours != null) && (ue != null) && (code != null)) {
            if (!mention.equals("Mention")) { // L'utilisateur a sélectionné de mention
                listeEnseignement = enseignementsPourMention(listeEnseignement, mention);
                if (!parcours.equals("Parcours")) {
                    listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                    if (!ue.equals("UE")) {
                        listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    } else {
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    }
                } else {
                    if (!ue.equals("UE")) {
                        listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    } else {
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    }
                }
                trierComboBoxParcours(mention, parcours);
                trierComboBoxNomM(mention, ue);
                trierComboBoxCodeM(mention, code);
            }
        }
        effectuerTrie(listeEnseignement);
    }

    /**
     * Trie les enseignements en fonctiond d'un parcours
     */
    public void trieParcours() {
        listeEnseignement = Enseignement.restaure(fichierEnseignement);

        // Récupérer valeur dans comboBox
        this.mention = (String) ComboBoxListeMention.getSelectionModel().getSelectedItem();
        this.parcours = (String) ComboBoxListeParcours.getSelectionModel().getSelectedItem();
        this.ue = (String) comboBoxListeUE.getSelectionModel().getSelectedItem();
        this.code = (String) comboBoxListeCodeUE.getSelectionModel().getSelectedItem();

        // Effectuer le trie
        if ((mention != null) && (parcours != null) && (ue != null) && (code != null)) {
            if (!parcours.equals("Parcours")) { 
                listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                if (!mention.equals("Mention")) {
                    listeEnseignement = enseignementsPourMention(listeEnseignement, mention);
                    if (!ue.equals("UE")) {
                        listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    } else {
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    }
                    trierComboBoxNomP(parcours, ue);
                    trierComboBoxCodeP(parcours, code);
                } else {
                    if (!ue.equals("UE")) {
                        listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    } else {
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                    }
                    trierComboBoxNomP(parcours, ue);
                    trierComboBoxCodeP(parcours, code);
                }
            }
        }
        effectuerTrie(listeEnseignement);
    }
    
    /**
     * Trie les enseignements en fonctiond d'un nom d'enseignement
     */
    public void trieNom() {
        listeEnseignement = Enseignement.restaure(fichierEnseignement);

        // Récupérer valeur dans comboBox
        this.mention = (String) ComboBoxListeMention.getSelectionModel().getSelectedItem();
        this.parcours = (String) ComboBoxListeParcours.getSelectionModel().getSelectedItem();
        this.ue = (String) comboBoxListeUE.getSelectionModel().getSelectedItem();
        this.code = (String) comboBoxListeCodeUE.getSelectionModel().getSelectedItem();

        // Effectuer le trie
        if ((mention != null) && (parcours != null) && (ue != null) && (code != null)) {
            if (!ue.equals("UE")) { 
                listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                if (!mention.equals("Mention")) {
                    listeEnseignement = enseignementsPourMention(listeEnseignement, mention);
                    if (!parcours.equals("Parcours")) {
                        listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                        trierComboBoxCodeN(ue, code);
                    } else {
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                        trierComboBoxCodeN(ue, code);
                    }
                } else {
                    if (!parcours.equals("Parcours")) {
                        listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                        trierComboBoxCodeN(ue, code);
                    } else {
                        if (!code.equals("Code UE")) {
                            listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                        }
                        trierComboBoxCodeN(ue, code);
                    }
                }                
            }
        }
        effectuerTrie(listeEnseignement);
    }
    
    /**
     * Trie les enseignements en fonctiond d'un code d'enseignement
     */
    public void trieCodeUE() {
        listeEnseignement = Enseignement.restaure(fichierEnseignement);

        // Récupérer valeur dans comboBox
        this.mention = (String) ComboBoxListeMention.getSelectionModel().getSelectedItem();
        this.parcours = (String) ComboBoxListeParcours.getSelectionModel().getSelectedItem();
        this.ue = (String) comboBoxListeUE.getSelectionModel().getSelectedItem();
        this.code = (String) comboBoxListeCodeUE.getSelectionModel().getSelectedItem();

        // Effectuer le trie
        if ((mention != null) && (parcours != null) && (ue != null) && (code != null)) {
            if (!code.equals("Code UE")) { 
                listeEnseignement = enseignementsPourCode(listeEnseignement, code);
                if (!mention.equals("Mention")) {
                    listeEnseignement = enseignementsPourMention(listeEnseignement, mention);
                    if (!parcours.equals("Parcours")) {
                        listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                        if (!ue.equals("UE")) {
                            listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        }
                    } else {
                        if (!ue.equals("UE")) {
                            listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        }
                    }
                } else {
                    if (!parcours.equals("Parcours")) {
                        listeEnseignement = enseignementsPourParcours(listeEnseignement, parcours);
                        if (!ue.equals("UE")) {
                            listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        }
                    } else {
                        if (!ue.equals("UE")) {
                            listeEnseignement = enseignementsPourNom(listeEnseignement, ue);
                        }
                    }
                }                
            }
        }
        effectuerTrie(listeEnseignement);
    }
    
    

    /**
     * afficher la page secInscription
     */
    public void tableAction() {
        table.setRowFactory(tv -> {
            TableRow<Enseignement> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Enseignement enseignement = row.getItem();
                    try {
                        Stage source = (Stage) table.getScene().getWindow();
                        source.close();
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/interfaceGraphique/fxml/SecInscription.fxml"));
                        Parent root = loader.load();

                        // Permet de sauvegarde les différents éléments d'un étudiant
                        SecInscriptionController pageSecInscription = loader.getController();
                        pageSecInscription.setRepertoire(repertoire);
                        pageSecInscription.setFichierEtudiant(fichierEtudiant);
                        pageSecInscription.setFichierEnseignement(fichierEnseignement);
                        pageSecInscription.setFichierMention(fichierMention);
                        pageSecInscription.setFichierParcours(fichierParcours);
                        pageSecInscription.initialiserLesRestaure();
                        pageSecInscription.setEnseignement(enseignement);
                        pageSecInscription.labelEnseignement(enseignement.getNom());
                        pageSecInscription.afficherListeEtudiantPeutSuivre();

                        // Ouvre la page PageDemandeDossier
                        Stage mainStage = new Stage();
                        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
                        mainStage.setTitle("Université");
                        mainStage.setScene(new Scene(root));
                        mainStage.setResizable(false);
                        mainStage.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });
            return row;
        });
    }

    /**
     * remettre les valeurs initialises des comboBox et textField
     */
    public void buttonAnnule() {
        ComboBoxListeMention.setValue("Mention");
        ComboBoxListeParcours.setValue("Parcours");
        comboBoxListeUE.setValue("UE");
        comboBoxListeCodeUE.setValue("Code UE");
        initialiserComboBox();
        initialiserTableauUE();
    }

    /**
     * afficher les boutons "se déconnecter", "modifier son mot de passe",
     * "modifier son identfiant"
     */
    public void afficherParametre() {
        panelParametre.setVisible(true);
        panelCacher.setVisible(true);
        buttonParametre.setVisible(false);
        buttonParametre1.setVisible(true);
    }

    /**
     * cacher les boutons "se déconnecter", "modifier son mot de passe",
     * "modifier son identfiant"
     */
    public void cacherParametre() {
        panelParametre.setVisible(false);
        panelCacher.setVisible(false);
        buttonParametre.setVisible(true);
        buttonParametre1.setVisible(false);
    }

    /**
     * se déconnecter
     *
     * @throws IOException
     */
    public void deconnexion() throws IOException {
        Stage source = (Stage) buttonDeconnexion.getScene().getWindow();
        source.close();
        Parent root = FXMLLoader.load(getClass().getResource("/interfaceGraphique/fxml/Page.fxml"));
        Stage mainStage = new Stage();
        Scene scene = new Scene(root);
        mainStage.getIcons().add(new Image("/ressources/images/logo.jpeg"));
        mainStage.setTitle("Université");
        mainStage.setResizable(false);
        mainStage.setScene(scene);
        mainStage.show();
    }
}
