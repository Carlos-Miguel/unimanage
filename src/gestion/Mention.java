package gestion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Décrit une Mention
 *
 * @author Prunevielle Océane, Gabrieli Laura, Correia De Almeida Carlos Miguel
 */
public class Mention implements Fichier {

    private String nom;
    private ArrayList<Enseignement> enseignements;
    private final static String CHEMINDEFAULT = "Données/mentions.csv";

    /**
     * Définit une Mention avec un nom (String)
     *
     * @param nom (String)
     */
    public Mention(String nom) {
        this.nom = nom;
        this.enseignements = new ArrayList<>();
    }

    /**
     * Définit une Mention avec un nom (String) et une liste d'Enseignement
     * (ArrayList&lt;Enseignement&gt;)
     *
     * @param nom (String)
     * @param ue (ArrayList&lt;Enseignement&gt;)
     */
    public Mention(String nom, ArrayList<Enseignement> ue) {
        this.nom = nom;
        this.enseignements = ue;
    }

    /**
     * Permet de récupérer le nom d'une Mention
     *
     * @return nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Permet de récupérer la liste des Enseignements
     *
     * @return ArrayList&lt;Enseignement&gt;
     */
    public ArrayList<Enseignement> getEnseignements() {
        return enseignements;
    }

    /**
     * Permet de modifier le nom d'une Mention
     *
     * @param n (String)
     */
    public void setNom(String n) {
        this.nom = n;
    }

    /**
     * Permet d'ajouter un Enseignement a une Mention
     *
     * @param ue (Enseignement)
     */
    public void ajoutEnseignement(Enseignement ue) {
        if (this.enseignements != null) {
            if (!this.enseignements.contains(ue)) {
                this.enseignements.add(ue);
            } else {
                System.out.println("Enseignement déjà dans la liste !");
            }
        } else {
            this.enseignements.add(ue);
        }

    }

    /**
     * Retourne les noms des mentions
     *
     * @param restaure (ArrayList&lt;Mention&gt;)
     * @return (ArrayList&lt;String&gt;)
     */
    public static ArrayList<String> nomMentions(ArrayList<Mention> restaure) {

        if (!restaure.isEmpty()) {
            ArrayList<String> noms = new ArrayList<>();
            for (int i = 0; i < restaure.size(); i++) {
                noms.add(restaure.get(i).getNom());
            }
            return noms;
        }
        return null;
    }

    /**
     * Permet de retrouver les enseignements d'une mention en connaissant sont
     * nom
     *
     * @param restaure (ArrayList&lt;Mention&gt;)
     * @param nomM (String)
     * @return (ArrayList&lt;Enseignement&gt;)
     */
    public static ArrayList<Enseignement> ensMention(ArrayList<Mention> restaure, String nomM) {
        if (!restaure.isEmpty()) {
            for (int i = 0; i < restaure.size(); i++) {
                if (restaure.get(i).getNom().equals(nomM)) {
                    return restaure.get(i).getEnseignements();
                }
            }
        }
        return null;
    }

    /**
     * Renvoie tout les noms des Enseignement d'une Mention (avec ces parcours)
     *
     * @param restaure
     * @param restaureParc
     * @param nomMention
     * @return
     */
    public static ArrayList<String> nomEnseignementPourMention(ArrayList<Mention> restaure, ArrayList<Parcours> restaureParc, String nomMention) {

        if (!restaure.isEmpty()) {
            ArrayList<Enseignement> ens = ensMention(restaure, nomMention);
            if (!restaureParc.isEmpty()) {
                ArrayList<Enseignement> ensTotal = new ArrayList<>();
                ensTotal.addAll(ens);
                for (Parcours p : restaureParc) {
                    if (p.getNom().trim().equals(nomMention.trim())) {
                        ensTotal.addAll(Parcours.listeEnsParcoursSansMention(restaureParc, p.getNomP()));
                    }
                }
                //return ens; => pour avoir une arraylist d'enseignement
                ArrayList<String> liste = new ArrayList<>();
                for (Enseignement ue : ensTotal) {
                    liste.add(ue.getNom());

                }
                return liste;
            }
        }

        return null;
    }

    /**
     * Renvoie tout les codes des Enseignements d'une Mention (avec ces
     * parcours)
     *
     * @param restaure
     * @param restaureParc
     * @param nomMention
     * @return
     */
    public static ArrayList<String> codeEnseignementPourMention(ArrayList<Mention> restaure, ArrayList<Parcours> restaureParc, String nomMention) {

        if (!restaure.isEmpty()) {
            ArrayList<Enseignement> ens = ensMention(restaure, nomMention);
            if (!restaureParc.isEmpty()) {
                ArrayList<Enseignement> ensTotal = new ArrayList<>();
                ensTotal.addAll(ens);
                for (Parcours p : restaureParc) {
                    if (p.getNom().trim().equals(nomMention.trim())) {
                        ensTotal.addAll(Parcours.listeEnsParcoursSansMention(restaureParc, p.getNomP()));
                    }
                }
                //return ens; => pour avoir une arraylist d'enseignement
                ArrayList<String> liste = new ArrayList<>();
                for (Enseignement ue : ensTotal) {
                    liste.add(ue.getCode());

                }
                return liste;
            }
        }

        return null;
    }

    

    private static String getPath() {
        String path = Mention.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        // Dans le .jar enleve "ProjetJava.jar" du chemin
        // sur l'ide enleve "/"
        String absolutePath = path.substring(0, path.lastIndexOf("/"));
        // Dans le .jar enleve "/dist" du chemin -> a enlever apres si on veux que les données
        // soit dans le meme dossier que le .jar
        // sur l'ide enleve "/classes"
        String[] verif = absolutePath.split("/");
        if (verif[verif.length - 1].equals("classes")) {
            absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            verif = absolutePath.split("/");
            //Si il y'a aussi "/build" l'enleve du chemin (pour l'ide)
            if (verif[verif.length - 1].equals("build")) {
                absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            }
        }
        absolutePath += "/";
        try {
            absolutePath = URLDecoder.decode(absolutePath, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Enseignement.class.getName()).log(Level.SEVERE, null, ex);
        }
        //absolutePath += CHEMINDEFAULT;
        //System.out.println("ABSOLU DE ENSEIGNEMENT: "+absolutePath);
        return absolutePath;

    }

    /**
     * Permet de sauvegarder une Mention dans un fichier mentions.csv Si une
     * modification à lieu la ligne concerné est modifier
     *
     */
    public void sauvegarde() {
        StringBuilder sb = new StringBuilder();
        StringBuilder tout = new StringBuilder();
        boolean dejaDansFich = false, uneModification = false;
        String path = getPath();
        // Vérifie si le répertoire Données existe sinon le crée
        File directory = new File(path + "Données");
        if (!directory.exists()) {
            directory.mkdir();
        }
        File f = new File(path + CHEMINDEFAULT);
        if (!f.exists()) {
            if (!f.isDirectory()) {
                sb.append('\ufeff');
                sb.append("Nom");
                sb.append(';');
                sb.append("Enseignements");
                sb.append('\n');
            }
        }
        sb.append(nom);
        sb.append(';');
        if (enseignements.isEmpty()) {
            sb.append("[]");
        } else {
            // Permet d'avoir un format plus facile à traiter
            for (int i = 0; i < enseignements.size(); i++) {
                sb.append("[");
                sb.append(enseignements.get(i).getCode());
                //sb.append(" , ");
                //sb.append(enseignements.get(i).getCredit());
                sb.append("]");
            }
        }

        BufferedReader br;
        if (f.exists()) {
            try {
                br = new BufferedReader(new InputStreamReader(new FileInputStream(path + CHEMINDEFAULT), "UTF-8"));
                String ligne;

                while ((ligne = br.readLine()) != null) {
                    if (ligne.equals(sb.toString())) {
                        dejaDansFich = true;
                    } else {
                        if (ligne.contains(nom)) {
                            System.out.println("ligne a modif");
                            tout.append(sb.toString());
                            tout.append("\n");
                            dejaDansFich = true;
                            uneModification = true;

                        } else {
                            tout.append(ligne);
                            tout.append("\n");
                        }

                    }
                }

            } catch (FileNotFoundException ex) {
                Logger.getLogger(Mention.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Mention.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (!dejaDansFich) {
            try {
                OutputStream out = new FileOutputStream(path + CHEMINDEFAULT, true);
                Writer writer = new OutputStreamWriter(out, "UTF-8");
                sb.append("\n");
                tout.append(sb.toString());
                writer.append(tout);
                writer.flush();
                //System.out.println("done!");
            } catch (IOException ex) {
                Logger.getLogger(Enseignement.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (dejaDansFich) {
                if (!uneModification) {
                    //System.out.println("Déjà dans le fichier ! ");
                } else {
                    if (uneModification) {
                        try {
                            OutputStream out = new FileOutputStream(path + CHEMINDEFAULT);
                            Writer writer = new OutputStreamWriter(out, "UTF-8");
                            writer.append(tout);
                            writer.flush();
                            //System.out.println("done!");;;
                        } catch (IOException ex) {
                            Logger.getLogger(Enseignement.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }
    }

    /**
     * Retourne une ArrayList&lt;Mention&gt; contenue dans le fichier contenue
     * dans les données du projet
     *
     * @param listeEuRestaurer (ArrayList&lt;Enseignement&gt;)
     * @return ArrayList&lt;Mention&gt;
     */
    public static ArrayList<Mention> restaure(ArrayList<Enseignement> listeEuRestaurer) {
        return Mention.restaure(listeEuRestaurer, getPath() + CHEMINDEFAULT);
    }

    /**
     * Retourne une ArrayList&lt;Mention&gt; contenue dans le fichier dont le
     * chemin et donner en paramètre !! les slashs doivent être doublée.
     *
     * @param listeEuRestaurer (ArrayList&lt;Enseignement&gt;)
     * @param chemin (String sous forme C:\\lechemin\\estvoila\\fichier.csv)
     * @return ArrayList&lt;Mention&gt;
     */
    public static ArrayList<Mention> restaure(ArrayList<Enseignement> listeEuRestaurer, String chemin) {
        if (listeEuRestaurer == null) {
            System.out.println("Le fichier des enseigneents est vide ! ");
            return null;

        }
        File f = new File(chemin);
        BufferedReader br;
        if (f.exists()) {
            if (!f.isDirectory()) {
                try {
                    br = new BufferedReader(new InputStreamReader(new FileInputStream(chemin), "UTF-8"));
                    String ligne, sansPare;
                    String[] colonne;
                    String[] verif;
                    boolean formatCorrect = false;
                    //ArrayList<Enseignement> listeEu = Enseignement.restaure();
                    ArrayList<Mention> liste = new ArrayList<>();

                    while ((ligne = br.readLine()) != null) {

                        colonne = ligne.split(";");

                        //transforme les [] en "" 
                        sansPare = colonne[1];
                        sansPare = sansPare.substring(1); // enleve le premier [
                        sansPare = sansPare.substring(0, sansPare.length() - 1);// enleve le dernier ]

                        if ((!colonne[0].equals("Nom") || !colonne[0].equals("\ufeffNom") || !colonne[0].equals("ï»¿Nom"))
                                && !colonne[1].equals("Enseignements") && formatCorrect) {
                            //System.out.println("Mention " + sansPare);
                            liste.add(new Mention(colonne[0]));
                            if (!"".equals(sansPare)) {
                                // il y a un prerequis a ajouter
                                verif = sansPare.split("]");

                                for (int i = 0; i < verif.length; i++) {
                                    // Rajoute les UE dans les mentions s'ils sont enregistrer dans la liste des enseignements
                                    for (int j = 0; j < listeEuRestaurer.size(); j++) {
                                        if (listeEuRestaurer.get(j).getCode().equals(verif[i].split(",")[0].trim())) {
                                            liste.get(liste.size() - 1).ajoutEnseignement(listeEuRestaurer.get(j));
                                        } else {
                                            if (listeEuRestaurer.get(j).getCode().equals(verif[i].split(",")[0].trim().substring(1))) {
                                                liste.get(liste.size() - 1).ajoutEnseignement(listeEuRestaurer.get(j));
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        if ((colonne[0].equals("\ufeffNom") || colonne[0].equals("Nom") || colonne[0].equals("ï»¿Nom"))
                                && colonne[1].equals("Enseignements")) {
                            formatCorrect = true;
                        }

                    }
                    if (!formatCorrect) {
                        //System.out.println("Le format des colonnes n'est pas correcte !");
                        return null;
                    }
                    return liste;

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Enseignement.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Enseignement.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            // System.out.println("Fichier n'existe pas");
            return null;
        }
        //System.out.println("Ce n'est pas un fichier");
        return null;
    }

    @Override
    public String toString() {
        return "Mention{" + "nom=" + nom + ", enseignement=" + enseignements + '}';
    }
}
