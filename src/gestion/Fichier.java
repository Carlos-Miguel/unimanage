package gestion;

/**
 * Décrit l'interface des fichiers .csv
 * 
 * @author Prunevielle Océane, Gabrieli Laura, Correia De Almeida Carlos Miguel
 */
public interface Fichier {

    /**
     * Servira a sauvegarder les fichiers .csv
     */
    static void sauvegarde() {

    }

    ;

    /**
     * Servira a restaurer un fichier .csv
     */
    static void restaure() {

    }

    ;
     /**
     * Servira a restaurer un fichier .csv avec un chemin spécifier (String)
     * @param chemin (String)
     */
    static void restaure(String chemin) {

    }
}
