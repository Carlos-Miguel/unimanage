package gestion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Décrit un Etudiant
 *
 * @author Prunevielle Océane, Gabrieli Laura, Correia De Almeida Carlos Miguel
 */
public class Etudiant implements Fichier {

    private int numE;
    private String nom;
    private String prenom;
    ArrayList<Suivi> listeSuivi = new ArrayList<>();
    private Parcours parcours;
    private final static String CHEMINDEFAULT = "Données/etudiants.csv";

    /**
     * Définit un Etudiant avec sont numE (int), nom (String), prenom(String),
     * parcours (Parcours)
     *
     * @param numE (int)
     * @param nom (String)
     * @param prenom (String)
     * @param parcours (Parcours)
     *
     */
    public Etudiant(int numE, String nom, String prenom, Parcours parcours) {
        this.numE = numE;
        this.nom = nom;
        this.prenom = prenom;
        this.parcours = parcours;
        for (int i = 0; i < parcours.getEnseignements().size(); i++) {
            //Création des suivis pour les cours du parcours
            //Création de l'année universitaire
            AnneeUniv annee = new AnneeUniv();
            //Création du nouveau suivi
            Suivi s = new Suivi(false, parcours.getEnseignements().get(i), annee);
            //Ajout du suivi dans la liste
            listeSuivi.add(s);

        }
        for (int i = 0; i < parcours.getEnseignementsP().size(); i++) {
            //Création des suivis pour les cours du parcours
            //Création de l'année universitaire
            AnneeUniv annee = new AnneeUniv();
            //Création du nouveau suivi
            Suivi s2 = new Suivi(false, parcours.getEnseignementsP().get(i), annee);
            listeSuivi.add(s2);
        }

    }

    /**
     * Définit un etudiant avec une liste de suivi déjà existante
     *
     * @param numE (int)
     * @param nom (String)
     * @param prenom (String)
     * @param parcours (Parcours)
     * @param listeSuivi (ArrayList&lt;Suivi&gt;)
     */
    public Etudiant(int numE, String nom, String prenom, Parcours parcours, ArrayList<Suivi> listeSuivi) {
        this.numE = numE;
        this.nom = nom;
        this.prenom = prenom;
        this.parcours = parcours;
        this.listeSuivi = listeSuivi;
    }

    /**
     * Permet de récupérer le numE d'un Etudiant
     *
     * @return numE
     */
    public int getNumE() {
        return numE;
    }

    /**
     * Permet de récupérer le nom d'un Etudiant
     *
     * @return nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Permet de récupérer le prenom d'un Etudiant
     *
     * @return prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Permet de récupérer la liste de Suivi d'un Etudiant
     *
     * @return ArrayList&lt;Suivi&gt;
     */
    public ArrayList<Suivi> getListeSuivi() {
        return listeSuivi;
    }

    /**
     * Permet de récupérer le Parcours d'un Etudiant
     *
     * @return Parcours
     */
    public Parcours getParcours() {
        return parcours;
    }

    /**
     * Permet de modifier le nom d'un Etudiant
     *
     * @param n (String)
     */
    public void setNom(String n) {
        this.nom = n;
    }

    /**
     * Permet de modifier le prenom d'un Etudiant
     *
     * @param prenom (String)
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * Permet de remplacer la liste de Suivi d'un Etudiant
     *
     * @param Suivi (Suivi)
     */
    public void setListeSuivi(ArrayList<Suivi> Suivi) {
        this.listeSuivi = Suivi;
    }

    /**
     * Permet d'ajouter un suivi a un étudiant
     *
     * @param s (Suivi)
     */
    public void ajoutSuivi(Suivi s) {
        this.listeSuivi.add(s);
    }

    /**
     * Permet de modifier le Parcours d'un Etudiant
     *
     * @param p (Parcours)
     */
    public void setParcours(Parcours p) {
        this.parcours = p;
    }

    /**
     * Renvoie la liste des enseignements validés
     *
     * @return (ArrayList&lt;Enseignement&gt;)
     */
    public ArrayList<Enseignement> ueValides() {
        ArrayList<Enseignement> valides = new ArrayList<>();
        for (int i = 0; i < listeSuivi.size(); i++) {
            if (listeSuivi.get(i).getValide()) {
                valides.add(listeSuivi.get(i).getEnseignement());
            }
        }
        return valides;
    }

    /**
     * True si l'Etudiant a validé l'enseignement
     *
     * @param e (Enseignement)
     * @return boolean
     */
    public boolean aValideEns(Enseignement e) {
        boolean b = false;
        for (int i = 0; i < ueValides().size(); i++) {
            if (ueValides().get(i).getCode().equals(e.getCode())) {
                b = true;
            }
        }
        return b;
    }

    /**
     *
     * @return la liste des enseignements en cours
     */
    public ArrayList<Enseignement> ueEnCours() {
        ArrayList<Enseignement> enCours = new ArrayList<>();
        AnneeUniv an = new AnneeUniv();
        //récupération de tous les enseignements suivis dans le semestre en cours
        for (int i = 0; i < listeSuivi.size(); i++) {
            if (listeSuivi.get(i).getAnnee().equals(an)) {
                if (listeSuivi.get(i).getSemestre().equals(an.getSemestreEnCours())) {
                    enCours.add(listeSuivi.get(i).getEnseignement());
                }
            }
        }
        return enCours;
    }


    /**
     * @param e (Enseignement)
     * @return true si l'Etudiant suit l'enseignement
     */
    public boolean suitEns(Enseignement e) {
        boolean b = false;
        for (int i = 0; i < this.ueEnCours().size(); i++) {
            if (this.ueEnCours().get(i).getCode().equals(e.getCode())) {
                b = true;
            }
        }
        return b;
    }

    /**
     * Retourne une liste d'étudiant en fonction d'un enseignement
     *
     * @param restaure (ArrayList&lt;Etudiant&gt;)
     * @param ensRestaurer (ArrayList&lt;Enseignement&gt;)
     * @param nomEns (String)
     * @return (ArrayList&lt;Etudiant&gt;)
     */
    public static ArrayList<Etudiant> listeEtudEns(ArrayList<Etudiant> restaure, ArrayList<Enseignement> ensRestaurer, String nomEns) {
        if (!restaure.isEmpty()) {
            if (!ensRestaurer.isEmpty()) {
                ArrayList<Etudiant> e = new ArrayList<>();
                //Peut y avoir plusieurs enseignements au meme nom
                ArrayList<Enseignement> ens = Enseignement.listeEnsPourNom(ensRestaurer, nomEns);
                for (int i = 0; i < restaure.size(); i++) {
                    for (int j = 0; j < ens.size(); j++) {
                        if (restaure.get(i).suitEns(ens.get(j))) {
                            e.add(restaure.get(i));
                        }
                    }
                }
                return e;
            }

        }
        return null;

    }

    /**
     * Retourne une liste d'Etudiant suivant un Enseignement dont le code est en
     * parametre
     *
     * @param restaure (ArrayList&lt;Etudiant&gt;)
     * @param ensRestaurer (ArrayList&lt;Enseignement&gt;)
     * @param code (String)
     * @return (ArrayList&lt;Etudiant&gt;)
     */
    public static ArrayList<Etudiant> listeEtudCode(ArrayList<Etudiant> restaure, ArrayList<Enseignement> ensRestaurer, String code) {
        if (!restaure.isEmpty()) {
            if (!ensRestaurer.isEmpty()) {
                ArrayList<Etudiant> etu = new ArrayList<>();
                // Un seul enseignement par code
                ArrayList<Enseignement> ens = Enseignement.listeEnsCode(ensRestaurer, code);
                for (int i = 0; i < restaure.size(); i++) {
                    if (restaure.get(i).suitEns(ens.get(0))) {
                        etu.add(restaure.get(i));
                    }

                }
                return etu;
            }
        }
        //System.out.println("ok");
        // Pas d'étudiant dans la liste 
        return null;
    }

    /**
     * Retourne la liste des Etudiants suivant un Enseignement
     *
     * @param restaure (ArrayList&lt;Etudiant&gt;)
     * @param e (Enseignement)
     * @return (ArrayList&lt;Etudiant&gt;)
     */
    public static ArrayList<Etudiant> listeEtudE(ArrayList<Etudiant> restaure, Enseignement e) {
        if (!restaure.isEmpty()) {
            ArrayList<Etudiant> ae = new ArrayList<>();
            for (int i = 0; i < restaure.size(); i++) {
                if (restaure.get(i).suitEns(e)) {
                    ae.add(restaure.get(i));
                }
            }
            return ae;
        }
        return null;
    }

    /**
     * Permet de vérifier si un Etudiant a les prérequis pour un Enseignement
     *
     * @param e (Enseignement)
     * @return true si il a les prérequis faux sinon
     */
    public boolean verifierPrerequis(Enseignement e) {
        ArrayList<Boolean> pr = new ArrayList<>();
        //Si l'enseignement a des prérequis
        if (e.getPrerequis() != null) {
            //on parcourt la liste de ces prérequis
            for (int i = 0; i < e.getPrerequis().size(); i++) {
                //on parcourt la liste du suivi de l'étudiant
                for (int j = 0; j < listeSuivi.size(); j++) {
                    //si l'étudiant a déjà validé le prérequis
                    if ((e.getPrerequis().get(i).getCode().equals(listeSuivi.get(j).getEnseignement().getCode())) && (this.listeSuivi.get(j).getValide())) {
                        //on ajoute vrai au tableau
                        pr.add(Boolean.TRUE);
                        break;
                    }
                }
            }
            //retourne vrai si l'étudiant a validé tous les prérequis
            return (e.getPrerequis().size() == pr.size());
        } else {
            //retourne vrai s'il n'y a pas de prérequis
            return true;
        }
    }

    /**
     * Permet d'ajouter un Enseignement à un Etudiant s'il ne l'a pas déjà
     * validé, qu'il n'est pas en train de le suivre et qu'il a les prérequis
     *
     * @param e (Enseignement)
     * @return (0 si ajouter, -3 si il suit déjà l'UE, -2 si il a déjà validé, -1 s'il n'a pas les prerequis)
     */
    public int ajouterEnseignement(Enseignement e) {
        //Vérification des prérequis
        if (verifierPrerequis(e)) {
            if(!aValideEns(e)){
                if(!suitEns(e)){
                    //Création de l'année universitaire
                    AnneeUniv annee = new AnneeUniv();
                    //Création du nouveau suivi
                    Suivi s = new Suivi(false, e, annee);
                    //Ajout du suivi dans la liste
                    listeSuivi.add(s);
                    return 0;
                }
                else{
                    // suit déjà l'UE
                    return -3;
                }
            }
            else{
                // a déjà validé l'UE
                return -2;
            }
        }
        else{
            // n'a pas les prérequis pour s'inscrire
            return -1;
        }
    }

    /**
     * @return la liste des enseignements non Validés
     */
    public ArrayList<Enseignement> ueNonValides() {
        ArrayList<Enseignement> nonValides = new ArrayList<>();
        for (int i = 0; i < listeSuivi.size(); i++) {
            if (!listeSuivi.get(i).getValide()) {
                nonValides.add(listeSuivi.get(i).getEnseignement());
            }
        }
        return nonValides;
    }

    /**
     * Retourne la liste des etudiants d'un parcours donnée en parametre
     *
     * @param restaure (ArrayList&lt;Etudiant&gt;)
     * @param nomParcours (String)
     * @return (ArrayList&lt;Etudiant&gt;)
     */
    public static ArrayList<Etudiant> listeEtuParcours(ArrayList<Etudiant> restaure, String nomParcours) {
        if (!restaure.isEmpty()) {
            ArrayList<Etudiant> liste = new ArrayList<>();
            for (Etudiant e : restaure) {
                if (e.getParcours().getNomP().equals(nomParcours)) {
                    liste.add(e);
                }
            }
            return liste;
        }
        return null;
    }

    /**
     * Retourne la liste des etudiants qui a comme numE celui en parametre (un
     * seul etudiant)
     *
     * @param restaure (ArrayList&lt;Etudiant&gt;)
     * @param num (int)
     * @return (ArrayList&lt;Etudiant&gt;)
     */
    public static ArrayList<Etudiant> listeEtuNumE(ArrayList<Etudiant> restaure, int num) {
        if (!restaure.isEmpty()) {
            ArrayList<Etudiant> liste = new ArrayList<>();
            for (Etudiant e : restaure) {
                if (e.getNumE() == num) {
                    liste.add(e);
                    return liste;
                }
            }
        }
        return null;
    }

    /**
     * Retourne la liste des etudiants qui ont le prenom en parametre
     *
     * @param restaure (ArrayList&lt;Etudiant&gt;)
     * @param prenomEtu (String)
     * @return (ArrayList&lt;Etudiant&gt;)
     */
    public static ArrayList<Etudiant> listeEtuPrenom(ArrayList<Etudiant> restaure, String prenomEtu) {
        if (!restaure.isEmpty()) {
            ArrayList<Etudiant> liste = new ArrayList<>();
            for (Etudiant e : restaure) {
                if (e.getPrenom().equals(prenomEtu)) {
                    liste.add(e);
                }
            }
            return liste;
        }
        return null;
    }

    /**
     * Retourne l'Etudiant dont le numero d'étudiant correspond à celui en parametre
     * @param restaure (ArrayList&lt;Etudiant&gt;)
     * @param num (int)
     * @return (Etudiant)
     */
    public static Etudiant listeEtuNumEBis(ArrayList<Etudiant> restaure, int num) {
        if (!restaure.isEmpty()) {
            System.out.println("test");
            ArrayList<Etudiant> liste = new ArrayList<>();
            for (Etudiant e : restaure) {
                if (e.getNumE() == num) {
                    return e;
                }
            }
        }
        System.out.println("test2");
        return null;
    }

    /**
     * Retourne la liste des etudiants qui ont le nom en parametre
     *
     * @param restaure(ArrayList&lt;Etudiant&gt;)
     * @param nomEtu (String)
     * @return (ArrayList&lt;Etudiant&gt;)
     */
    public static ArrayList<Etudiant> listeEtuNom(ArrayList<Etudiant> restaure, String nomEtu) {
        if (!restaure.isEmpty()) {
            ArrayList<Etudiant> liste = new ArrayList<>();
            for (Etudiant e : restaure) {
                if (e.getNom().equals(nomEtu)) {
                    liste.add(e);
                }
            }
            return liste;
        }
        return null;
    }

    /**
     * Retourne la liste des etudiants qui ont pour mention celle en parametre
     *
     * @param restaure (ArrayList&lt;Etudiant&gt;)
     * @param nomMention (String)
     * @return (ArrayList&lt;Etudiant&gt;)
     */
    public static ArrayList<Etudiant> listeEtuMention(ArrayList<Etudiant> restaure, String nomMention) {
        if (!restaure.isEmpty()) {
            ArrayList<Etudiant> liste = new ArrayList<>();
            for (Etudiant e : restaure) {
                if (e.getParcours().getNom().equals(nomMention)) {
                    liste.add(e);
                }
            }
            return liste;
        }
        return null;

    }

    /**
     * Renvoyer la liste des enseignements possible pour un etudiant
     *
     * @param restaure (ArrayList&lt;Enseignement&gt;)
     * @return (ArrayList&lt;Enseignement&gt;)
     */
    public ArrayList<Enseignement> listeEnsPossible(ArrayList<Enseignement> restaure) {
        if (!restaure.isEmpty()) {
            ArrayList<Enseignement> liste = new ArrayList<>();
            for (Enseignement ens : restaure) {
                if (this.verifierPrerequis(ens)) {
                    liste.add(ens);
                }
            }
            Iterator<Enseignement> it = liste.iterator();
            while (it.hasNext()) {
                Enseignement i = it.next();
                if (this.aValideEns(i) || this.suitEns(i)) {
                    it.remove();
                }
            }
            return liste;
        }
        return null;
    }

    /**
     * Permet de recuperer les enseignements possible pour un etudiant et une
     * mention donnée
     *
     * @param restaure (ArrayList&lt;Enseignement&gt;)
     * @param restaureMention (ArrayList&lt;Mention&gt;)
     * @param restaureParcours (ArrayList&lt;Parcours&gt;)
     * @param nomMention (String)
     * @return (ArrayList&lt;Enseignement&gt;)
     */
    public ArrayList<Enseignement> listeEnsPossibleMention(ArrayList<Enseignement> restaure,
            ArrayList<Mention> restaureMention,
            ArrayList<Parcours> restaureParcours,
            String nomMention) {
        if (!restaure.isEmpty()) {
            if (!restaureMention.isEmpty()) {
                if (!restaureParcours.isEmpty()) {

                    ArrayList<Enseignement> liste = new ArrayList<>();
                    ArrayList<Enseignement> listeEnsParcoursDansMention = new ArrayList<>();
                    ArrayList<Enseignement> listeEnsPossible;
                    listeEnsPossible = this.listeEnsPossible(restaure);
                    ArrayList<Enseignement> listeEnsMention = Mention.ensMention(restaureMention, nomMention);

                    // Parcours la liste des enseignements pour laquel l'etudiant a les prerequis
                    // et si elles sont dans la liste des enseignements de la mention les ajoute a la liste a
                    // return
                    for (Enseignement ens : listeEnsPossible) {
                        for (Enseignement ens2 : listeEnsMention) {
                            if (ens.getCode().equals(ens2.getCode())) {
                                liste.add(ens);
                            }
                        }
                    }
                    // Recupere tout les enseignements des parcours qui ont pour mention
                    //celui rentrer en parametre
                    for (Parcours p1 : restaureParcours) {
                        if (p1.getNom().equals(nomMention)) {
                            for (int i = 0; i < p1.getEnseignementsP().size(); i++) {
                                if (!listeEnsParcoursDansMention.contains(p1.getEnseignementsP().get(i))) {
                                    listeEnsParcoursDansMention.add(p1.getEnseignementsP().get(i));
                                }
                            }
                        }

                    }
                    for (Enseignement ens : listeEnsPossible) {
                        for (Enseignement ens2 : listeEnsParcoursDansMention) {
                            if (ens.getCode().equals(ens2.getCode())) {
                                if (!liste.contains(ens)) {
                                    liste.add(ens);
                                }

                            }
                        }
                    }
                    return liste;
                }
            }

        }

        return null;

    }

    /**
     * Permet de recuperer les enseignements possible d'un etudiant pour un
     * parcours donnée
     *
     * @param restaure (ArrayList&lt;Enseignement&gt;)
     * @param restaureParcours (ArrayList&lt;Parcours&gt;)
     * @param nomParcours (String)
     * @return (ArrayList&lt;Enseignement&gt;)
     */
    public ArrayList<Enseignement> listeEnsPossibleParcours(ArrayList<Enseignement> restaure,
            ArrayList<Parcours> restaureParcours, String nomParcours) {
        if (!restaure.isEmpty()) {
            if (!restaureParcours.isEmpty()) {
                ArrayList<Enseignement> liste = new ArrayList<>();
                ArrayList<Enseignement> listeEnsPossible = this.listeEnsPossible(restaure);
                ArrayList<Enseignement> listeEnsParcours = Parcours.listeEnsParcoursSansMention(restaureParcours, nomParcours);
                for (Enseignement ens : listeEnsPossible) {
                    for (Enseignement ens2 : listeEnsParcours) {
                        if (ens.getCode().equals(ens2.getCode())) {
                            liste.add(ens);
                        }
                    }
                }
                return liste;
            }

        }

        return null;

    }

   private static String getPath() {
        String path = Etudiant.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        // Dans le .jar enleve "ProjetJava.jar" du chemin
        // sur l'ide enleve "/"
        String absolutePath = path.substring(0, path.lastIndexOf("/"));
        // Dans le .jar enleve "/dist" du chemin -> a enlever apres si on veux que les données
        // soit dans le meme dossier que le .jar 
        // sur l'ide enleve "/classes"
        String[] verif = absolutePath.split("/");
        if (verif[verif.length - 1].equals("classes")) {
            absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            verif = absolutePath.split("/");
            //Si il y'a aussi "/build" l'enleve du chemin (pour l'ide)
            if (verif[verif.length - 1].equals("build")) {
                absolutePath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            }
        }
        absolutePath += "/";
         try {
            absolutePath=URLDecoder.decode(absolutePath,"UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Enseignement.class.getName()).log(Level.SEVERE, null, ex);
        }  
        //absolutePath += CHEMINDEFAULT;
        //System.out.println("ABSOLU DE ENSEIGNEMENT: "+absolutePath);
        return absolutePath;

    }

    /**
     * Sauvegarde les étudiants
     */
    public void sauvegarde() {
        StringBuilder sb = new StringBuilder();
        StringBuilder tout = new StringBuilder();
        boolean dejaDansFich = false, uneModification = false;
        String[] colonne;
        String path = getPath();
        // Vérifie si le répertoire Données existe sinon le crée
        File directory = new File(path + "Données");
        if (!directory.exists()) {
            directory.mkdir();
        }

        File f = new File(path + CHEMINDEFAULT);
        if (!f.exists()) {
            if (!f.isDirectory()) {
                sb.append('\ufeff');
                sb.append("NumE");
                sb.append(';');
                sb.append("Nom");
                sb.append(';');
                sb.append("Prenom");
                sb.append(';');
                sb.append("Parcours");
                sb.append(';');
                sb.append("listeSuivi");
                sb.append('\n');
            }
        }
        sb.append(numE);
        sb.append(';');
        sb.append(nom);
        sb.append(';');
        sb.append(prenom);
        sb.append(';');
        sb.append(parcours.getNomP());
        sb.append(";");
        if (listeSuivi.isEmpty()) {
            sb.append("[]");
        } else {
            // Permet d'avoir un format plus facile à traiter
            for (int i = 0; i < listeSuivi.size(); i++) {
                sb.append("[");
                sb.append(listeSuivi.get(i).getEnseignement().getCode());
                sb.append(" , ");
                sb.append(listeSuivi.get(i).getEnseignement().getCredit());
                sb.append(" , ");
                sb.append(listeSuivi.get(i).getAnnee().getAnnee());
                sb.append(" , ");
                sb.append(listeSuivi.get(i).getSemestre());
                sb.append(" , ");
                sb.append(listeSuivi.get(i).getValide());
                sb.append("]");
            }
        }

        BufferedReader br;
        if (f.exists()) {
            try {
                //br = new BufferedReader(new FileReader(path + CHEMINDEFAULT));
                br = new BufferedReader(new InputStreamReader(new FileInputStream(path+CHEMINDEFAULT), "UTF-8"));

                String ligne;

                while ((ligne = br.readLine()) != null) {
                    colonne = ligne.split(";");
                    if (ligne.equals(sb.toString())) {
                        dejaDansFich = true;
                    } else {
                        if (colonne[0].equals(String.valueOf(numE))) {
                            //System.out.println("ligne a modif");
                            tout.append(sb.toString());
                            tout.append("\n");
                            dejaDansFich = true;
                            uneModification = true;

                        } else {
                            tout.append(ligne);
                            tout.append("\n");
                        }

                    }
                }

            } catch (FileNotFoundException ex) {
                Logger.getLogger(Etudiant.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Etudiant.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (!dejaDansFich) {
            try {
                //FileWriter writer = new FileWriter(path + CHEMINDEFAULT);
                OutputStream out = new FileOutputStream(path+CHEMINDEFAULT,false);
                Writer writer = new OutputStreamWriter(out,"UTF-8");
                sb.append('\n');
                tout.append(sb.toString());
                writer.append(tout);
                writer.flush();
                //System.out.println("done!");
            } catch (IOException ex) {
                Logger.getLogger(Etudiant.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (dejaDansFich) {
                if (!uneModification) {
                    //System.out.println("Déjà dans le fichier ! ");
                } else {
                    try {
                        //FileWriter writer = new FileWriter(path + CHEMINDEFAULT);
                        OutputStream out = new FileOutputStream(path+CHEMINDEFAULT);
                        Writer writer = new OutputStreamWriter(out,"UTF-8");
                        writer.append(tout);
                        writer.flush();
                        //System.out.println("done!");;;
                    } catch (IOException ex) {
                        Logger.getLogger(Etudiant.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }

    }

    /**
     * Retourne une ArrayList&lt;Etudiant&gt; contenue dans le fichier contenue
     * dans les données du projet
     *
     * @param listeEuRestaurer (ArrayList&lt;Enseignement&gt;)
     * @param listeParRestaurer (ArrayList&lt;Parcours&gt;)
     * @return ArrayList&lt;Etudiant&gt;
     */
    public static ArrayList<Etudiant> restaure(ArrayList<Enseignement> listeEuRestaurer,
            ArrayList<Parcours> listeParRestaurer) {
        return Etudiant.restaure(listeEuRestaurer, listeParRestaurer, getPath() + CHEMINDEFAULT);
    }

    /**
     * Retourne une ArrayList&lt;Etudiant&gt; contenue dans le fichier dont le
     * chemin et donner en paramètre !! les slashs doivent être doublée.
     *
     * @param listeEuRestaurer (ArrayList&lt;Enseignement&gt;)
     * @param listeParRestaurer (ArrayList&lt;Parcours&gt;)
     * @param chemin (String sous forme C:\\lechemin\\estvoila\\fichier.csv)
     * @return ArrayList&lt;Mention&gt;
     */
    public static ArrayList<Etudiant> restaure(ArrayList<Enseignement> listeEuRestaurer,
            ArrayList<Parcours> listeParRestaurer, String chemin) {
        if (listeEuRestaurer == null) {
            System.out.println("La liste Enseignements est null !");
            return null;
        }
        if (listeParRestaurer == null) {
            System.out.println("La liste Parcours est null !");
            return null;
        }
        File f = new File(chemin);
        BufferedReader br;
        if (f.exists()) {
            if (!f.isDirectory()) {
                try {
                    br = new BufferedReader(new InputStreamReader(new FileInputStream(chemin), "UTF-8"));
                    String ligne, sansPare;
                    String[] colonne;
                    String[] verif;
                    String[] decompo;
                    boolean formatCorrect = false;

                    // A METTRE EN PARAM ??
                    //ArrayList<Enseignement> listeEu = Enseignement.restaure();
                    //ArrayList<Parcours> listePar = Parcours.restaureP();
                    ArrayList<Etudiant> liste = new ArrayList<>();

                    while ((ligne = br.readLine()) != null) {
                        ArrayList<Suivi> listeSuiv = new ArrayList<>();
                        colonne = ligne.split(";");

                        //transforme les [] en "" 
                        sansPare = colonne[4];
                        sansPare = sansPare.substring(1); // enleve le premier [
                        sansPare = sansPare.substring(0, sansPare.length() - 1);// enleve le dernier ]

                        if ((!colonne[0].equals("NumE") || !colonne[0].equals("\ufeffNumE") || !colonne[0].equals("ï»¿NumE")) && !colonne[1].equals("Nom") && !colonne[2].equals("Prenom")
                                && !colonne[3].equals("Parcours") && !colonne[4].equals("listeSuivi") && formatCorrect) {
                            if (!"".equals(sansPare)) {
                                //Suivi(boolean valide, Enseignement ens, AnneeUniv an, String sem)
                                // verif [i] => tout un suivi
                                verif = sansPare.split("]");
                                for (int i = 0; i < verif.length; i++) {

                                    decompo = verif[i].split(",");
                                    // decompo [0] = nom ens
                                    //decompo [1] = credit ens NON necessaire
                                    // decompo [2] = an
                                    // decompo [3] = sem
                                    // decompo [4] boolean

                                    for (int j = 0; j < listeEuRestaurer.size(); j++) {
                                        if (listeEuRestaurer.get(j).getCode().equals(verif[i].split(",")[0].trim())) {
                                            //liste.get(liste.size() - 1).ajoutEnseignement(listeEu.get(j));
                                            listeSuiv.add(new Suivi(Boolean.parseBoolean(decompo[4].trim()), listeEuRestaurer.get(j), new AnneeUniv(decompo[2].trim()), decompo[3].trim()));
                                            // System.out.println("TESTTTTT XXXXXXXXX " + Boolean.parseBoolean(decompo[4].trim()));
                                            //    System.out.println("decompo 4 "+decompo[4]);

                                        } else {
                                            if (listeEuRestaurer.get(j).getCode().equals(verif[i].split(",")[0].trim().substring(1))) {
                                                listeSuiv.add(new Suivi(Boolean.parseBoolean(decompo[4].trim()), listeEuRestaurer.get(j), new AnneeUniv(decompo[2].trim()), decompo[3].trim()));

                                            }
                                        }
                                    }
                                }
                            }
                            for (int i = 0; i < listeParRestaurer.size(); i++) {
                                if (listeParRestaurer.get(i).getNomP().equals(colonne[3])) {
                                    liste.add(new Etudiant(Integer.parseInt(colonne[0]), colonne[1],
                                            colonne[2], listeParRestaurer.get(i), listeSuiv));

                                }
                            }

                        }
                        if ((colonne[0].equals("NumE") || colonne[0].equals("\ufeffNumE") || colonne[0].equals("ï»¿NumE"))
                                && colonne[1].equals("Nom") && colonne[2].equals("Prenom")
                                && colonne[3].equals("Parcours") && colonne[4].equals("listeSuivi")) {
                            formatCorrect = true;
                        }

                    }
                    if (!formatCorrect) {
                        //System.out.println("Le format des colonnes n'est pas correcte !");
                        return null;
                    }
                    return liste;

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Enseignement.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Enseignement.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            // System.out.println("Fichier n'existe pas");
            return null;
        }
        //System.out.println("Ce n'est pas un fichier");
        return null;
    }

    @Override
    public String toString() {
        return "Etudiant{" + "numE=" + numE + ", nom=" + nom + ", prenom=" + prenom + ", listeSuivi=" + listeSuivi + ", parcours=" + parcours + '}';
    }

}
